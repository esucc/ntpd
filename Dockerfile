FROM alpine:latest
RUN apk update
RUN apk add openntpd
EXPOSE 123/udp
ENTRYPOINT ["/usr/sbin/ntpd", "-v", "-d", "-s"]
